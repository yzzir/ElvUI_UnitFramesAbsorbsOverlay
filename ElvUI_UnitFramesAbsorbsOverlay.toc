## Interface: 70200
## Author: Yzzir
## Version: 1.0
## Title: |cff1784d1ElvUI|r UnitFrames Absorbs Overlay
Notes: ElvUI plugin modifying how absorbs are displayed on unitframes.
## RequiredDeps: ElvUI

main.lua
