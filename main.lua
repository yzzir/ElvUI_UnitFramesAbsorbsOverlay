--[[
	This is a framework showing how to create a plugin for ElvUI.
	It creates some default options and inserts a GUI table to the ElvUI Config.
	If you have questions then ask in the Tukui lua section: http://www.tukui.org/forums/forum.php?id=27
]]

local E, L, V, P, G = unpack(ElvUI); --Import: Engine, Locales, PrivateDB, ProfileDB, GlobalDB
local UFAO = E:NewModule('UnitFramesAbsorbsOverlay', 'AceHook-3.0', 'AceEvent-3.0', 'AceTimer-3.0'); --Create a plugin within ElvUI and adopt AceHook-3.0, AceEvent-3.0 and AceTimer-3.0. We can make use of these later.
local EP = LibStub("LibElvUIPlugin-1.0") --We can use this to automatically insert our GUI tables when ElvUI_Config is loaded.
local addonName, addonTable = ... --See http://www.wowinterface.com/forums/showthread.php?t=51502&p=304704&postcount=2
local EUF = E:GetModule('UnitFrames');
local min = math.min;
--Default options
P["UFAO"] = {
	["Enable"] = false,
}

local function UpdateFillBar(frame, previousTexture, bar, amount, inverted, flipped)
	if ( amount == 0 ) then
		bar:Hide();
		return previousTexture;
	end

	local hanchor = "RIGHT";
	local vanchor = "TOP";
	if (flipped) then
		hanchor = "LEFT";
		vanchor = "BOTTOM";
	end

	local orientation = frame.Health:GetOrientation()
	bar:ClearAllPoints()
	if orientation == 'HORIZONTAL' then
		if (inverted) then
				bar:Point("TOPRIGHT", previousTexture, "TOP"..hanchor);
				bar:Point("BOTTOMRIGHT", previousTexture, "BOTTOM"..hanchor);
		else
			bar:Point("TOPLEFT", previousTexture, "TOP"..hanchor);
			bar:Point("BOTTOMLEFT", previousTexture, "BOTTOM"..hanchor);
		end
	else
		if (inverted) then
			bar:Point("TOPRIGHT", previousTexture, vanchor.."RIGHT");
			bar:Point("TOPLEFT", previousTexture, vanchor.."LEFT");
		else
			bar:Point("BOTTOMRIGHT", previousTexture, vanchor.."RIGHT");
			bar:Point("BOTTOMLEFT", previousTexture, vanchor.."LEFT");
		end
	end

	local totalWidth, totalHeight = frame.Health:GetSize();
	if orientation == 'HORIZONTAL' then
		bar:Width(totalWidth);
	else
		bar:Height(totalHeight);
	end

	return bar:GetStatusBarTexture();
end

function UFAO:PostUpdate(unit, myIncomingHeal, allIncomingHeal, totalAbsorb, healAbsorb)
	local frame = self.parent
	local previousTexture = frame.Health:GetStatusBarTexture();

	local newTotalAbsorb = min(UnitGetTotalAbsorbs(unit), UnitHealthMax(unit)) or 0;
	self.absorbBar:SetValue(newTotalAbsorb);
	UpdateFillBar(frame, previousTexture, self.absorbBar, newTotalAbsorb, false, true);

	UpdateFillBar(frame, previousTexture, self.healAbsorbBar, healAbsorb, true);
	previousTexture = UpdateFillBar(frame, previousTexture, self.myBar, myIncomingHeal);
	previousTexture = UpdateFillBar(frame, previousTexture, self.otherBar, allIncomingHeal);
end

function UFAO:Configure_HealthBar(frame)
	if (frame.HealPrediction and frame.HealPrediction.PostUpdate and E.db.UFAO.Enable) then
		frame.HealPrediction.PostUpdate = UFAO.PostUpdate;
	end
end

--This function inserts our GUI table into the ElvUI Config. You can read about AceConfig here: http://www.wowace.com/addons/ace3/pages/ace-config-3-0-options-tables/
function UFAO:InsertOptions()
	E.Options.args.unitframe.args.general.args.allColorsGroup.args.healPrediction.args.UFAO = {
		order = 5,
		type = "toggle",
		name = ("|cffff8000%s|r"):format("Unit Frames Absorbs Overlay"),
		get = function(info)
			return E.db.UFAO.Enable;
		end,
		set = function(info, value)
			E.db.UFAO.Enable = value;
			E:StaticPopup_Show("CONFIG_RL");
		end,
	}
end

function UFAO:Initialize()
	hooksecurefunc(EUF, "Configure_HealthBar", UFAO.Configure_HealthBar)

	--Register plugin so options are properly inserted when config is loaded
	EP:RegisterPlugin(addonName, UFAO.InsertOptions)
end

E:RegisterModule(UFAO:GetName()) --Register the module with ElvUI. ElvUI will now call UFAO:Initialize() when ElvUI is ready to load our plugin.
